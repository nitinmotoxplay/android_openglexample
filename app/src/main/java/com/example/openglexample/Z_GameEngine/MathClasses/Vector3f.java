package com.example.openglexample.Z_GameEngine.MathClasses;

import com.example.openglexample.Helper.HelperFunctions;

/**
 * Created by Nitin Khurana on 06/11/18.
 */
public class Vector3f {

    public float x, y, z;

    public Vector3f(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double length() {
        double sum = Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2);
        return Math.sqrt(sum);
    }

    public void normalize() {

        double length = length();
        x /= length;
        y /= length;
        z /= length;
    }

    public void print() {
        HelperFunctions.showLog("x : " + x + " y : " + y + " z : " + z);
    }
}
