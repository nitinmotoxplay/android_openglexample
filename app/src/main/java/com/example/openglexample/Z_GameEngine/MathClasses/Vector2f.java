package com.example.openglexample.Z_GameEngine.MathClasses;

import com.example.openglexample.Helper.HelperFunctions;

/**
 * Created by Nitin Khurana on 06/11/18.
 */
public class Vector2f {

    public float x, y;

    public Vector2f(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void print() {
        HelperFunctions.showLog("x : " + x + " y : " + y);
    }
}
