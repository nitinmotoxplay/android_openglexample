package com.example.openglexample.Z_GameEngine.Models;

import android.content.Context;

import com.example.openglexample.R;
import com.example.openglexample.Z_GameEngine.Entities.Entity;
import com.example.openglexample.Z_GameEngine.RenderEngine.Loader;
import com.example.openglexample.Z_GameEngine.RenderEngine.OBJLoader.ModelData;
import com.example.openglexample.Z_GameEngine.RenderEngine.OBJLoader.OBJFileLoader;
import com.example.openglexample.Z_GameEngine.Textures.ModelTexture;

/**
 * Created by Nitin Khurana on 04/11/18.
 */
public class SampleModels {

    public static Entity createStall(Context context, float[] position, float[] rotation, Loader loader) {

        ModelData modelData = OBJFileLoader.loadOBJ(context, "Models/stall.obj");
        RawModel rawModel = loader.loadToVao(modelData.getVertices(), modelData.getTextureCoords(), modelData.getNormals(), modelData.getIndices());

        ModelTexture texture = new ModelTexture(loader.loadTexture(context, R.drawable.stall));

        TexturedModel staticModel = new TexturedModel(rawModel, texture);

        return new Entity(staticModel, position, rotation, 1);
    }

    public static Entity createLamp(Context context, float[] position, float[] rotation, Loader loader) {

        ModelData modelData = OBJFileLoader.loadOBJ(context, "Models/lamp.obj");
        RawModel rawModel = loader.loadToVao(modelData.getVertices(), modelData.getTextureCoords(), modelData.getNormals(), modelData.getIndices());

        ModelTexture texture = new ModelTexture(loader.loadTexture(context, R.drawable.lamp));

        TexturedModel staticModel = new TexturedModel(rawModel, texture);

        return new Entity(staticModel, position, rotation, 1);
    }

    public static Entity createGrass(Context context, float[] position, float[] rotation, Loader loader) {

        ModelData modelData = OBJFileLoader.loadOBJ(context, "Models/grass.obj");
        RawModel rawModel = loader.loadToVao(modelData.getVertices(), modelData.getTextureCoords(), modelData.getNormals(), modelData.getIndices());

        ModelTexture texture = new ModelTexture(loader.loadTexture(context, R.drawable.grass));
        texture.setUseFakeLighting(true);
        texture.setHasTransparency(true);

        TexturedModel staticModel = new TexturedModel(rawModel, texture);

        return new Entity(staticModel, position, rotation, 1);
    }

    public static Entity createFlower(Context context, float[] position, float[] rotation, Loader loader) {

        ModelData modelData = OBJFileLoader.loadOBJ(context, "Models/grass.obj");
        RawModel rawModel = loader.loadToVao(modelData.getVertices(), modelData.getTextureCoords(), modelData.getNormals(), modelData.getIndices());

        ModelTexture texture = new ModelTexture(loader.loadTexture(context, R.drawable.flower));
        texture.setUseFakeLighting(true);
        texture.setHasTransparency(true);

        TexturedModel staticModel = new TexturedModel(rawModel, texture);

        return new Entity(staticModel, position, rotation, 1);
    }

    public static Entity createFern(Context context, float[] position, float[] rotation, Loader loader) {

        ModelData modelData = OBJFileLoader.loadOBJ(context, "Models/fern.obj");
        RawModel rawModel = loader.loadToVao(modelData.getVertices(), modelData.getTextureCoords(), modelData.getNormals(), modelData.getIndices());

        ModelTexture texture = new ModelTexture(loader.loadTexture(context, R.drawable.fern));
        texture.setUseFakeLighting(true);
        texture.setHasTransparency(true);
        texture.setNumberOfRows(2);

        TexturedModel staticModel = new TexturedModel(rawModel, texture);

        return new Entity(staticModel, position, rotation, 1);
    }

    public static Entity createTree(Context context, float[] position, float[] rotation, Loader loader) {

        ModelData modelData = OBJFileLoader.loadOBJ(context, "Models/tree.obj");
        RawModel rawModel = loader.loadToVao(modelData.getVertices(), modelData.getTextureCoords(), modelData.getNormals(), modelData.getIndices());

        ModelTexture texture = new ModelTexture(loader.loadTexture(context, R.drawable.tree));

        TexturedModel staticModel = new TexturedModel(rawModel, texture);

        return new Entity(staticModel, position, rotation, 4);
    }

    public static Entity createLowPolyTree(Context context, float[] position, float[] rotation, Loader loader) {

        ModelData modelData = OBJFileLoader.loadOBJ(context, "Models/lowPolyTree.obj");
        RawModel rawModel = loader.loadToVao(modelData.getVertices(), modelData.getTextureCoords(), modelData.getNormals(), modelData.getIndices());

        ModelTexture texture = new ModelTexture(loader.loadTexture(context, R.drawable.lowpolytree));

        TexturedModel staticModel = new TexturedModel(rawModel, texture);

        return new Entity(staticModel, position, rotation, 0.5f);
    }


    public static Entity createDragon(Context context, float[] position, Loader loader) {
        ModelData modelData = OBJFileLoader.loadOBJ(context, "Models/dragon.obj");
        RawModel rawModel = loader.loadToVao(modelData.getVertices(), modelData.getTextureCoords(), modelData.getNormals(), modelData.getIndices());

        ModelTexture texture = new ModelTexture(loader.loadTexture(context, R.drawable.brass));

        texture.setShineDamper(10);
        texture.setReflectivity(0.4f);

        TexturedModel staticModel = new TexturedModel(rawModel, texture);

        return new Entity(staticModel, position, new float[] {0, 0, 0}, 1);

    }

    public static Entity createQuad(Context context, float[] position, Loader loader) {

        float[] vertices = {

                -0.5f, 0.5f, 0f,
                -0.5f, -0.5f, 0f,
                0.5f, -0.5f, 0f,
                0.5f, 0.5f, 0f,

        };

        int[] indices = {
                0, 1, 3,
                3, 1, 2,

        };

        float[] textureCoords = {

                0,0,
                0,1,
                1,1,
                1,0
        };

        float[] normals = {

                -0.5f, 0.5f, 0.5f,
                -0.5f, -0.5f, 0.5f,
                0.5f, -0.5f, 0.5f,
                0.5f, 0.5f, 0.5f,

        };

        RawModel rawModel = loader.loadToVao(vertices, textureCoords, normals, indices);

        ModelTexture texture = new ModelTexture(loader.loadTexture(context, R.drawable.sample));
        texture.setReflectivity(1);
        texture.setShineDamper(10);

        TexturedModel staticModel = new TexturedModel(rawModel, texture);

        return new Entity(staticModel, position, new float[] {0, 0, 0}, 1);
    }

    public static Entity createTexturedCube(Context context, float[] position, Loader loader) {

        float[] vertices = {
                -0.5f,0.5f,-0.5f,
                -0.5f,-0.5f,-0.5f,
                0.5f,-0.5f,-0.5f,
                0.5f,0.5f,-0.5f,

                -0.5f,0.5f,0.5f,
                -0.5f,-0.5f,0.5f,
                0.5f,-0.5f,0.5f,
                0.5f,0.5f,0.5f,

                0.5f,0.5f,-0.5f,
                0.5f,-0.5f,-0.5f,
                0.5f,-0.5f,0.5f,
                0.5f,0.5f,0.5f,

                -0.5f,0.5f,-0.5f,
                -0.5f,-0.5f,-0.5f,
                -0.5f,-0.5f,0.5f,
                -0.5f,0.5f,0.5f,

                -0.5f,0.5f,0.5f,
                -0.5f,0.5f,-0.5f,
                0.5f,0.5f,-0.5f,
                0.5f,0.5f,0.5f,

                -0.5f,-0.5f,0.5f,
                -0.5f,-0.5f,-0.5f,
                0.5f,-0.5f,-0.5f,
                0.5f,-0.5f,0.5f

        };

        float[] textureCoords = {

                0,0,
                0,1,
                1,1,
                1,0,
                0,0,
                0,1,
                1,1,
                1,0,
                0,0,
                0,1,
                1,1,
                1,0,
                0,0,
                0,1,
                1,1,
                1,0,
                0,0,
                0,1,
                1,1,
                1,0,
                0,0,
                0,1,
                1,1,
                1,0


        };

        int[] indices = {
                0,1,3,
                3,1,2,
                4,5,7,
                7,5,6,
                8,9,11,
                11,9,10,
                12,13,15,
                15,13,14,
                16,17,19,
                19,17,18,
                20,21,23,
                23,21,22

        };

        RawModel rawModel = loader.loadToVao(vertices, textureCoords, new float[vertices.length], indices);

        ModelTexture texture = new ModelTexture(loader.loadTexture(context, R.drawable.box));

        TexturedModel staticModel = new TexturedModel(rawModel, texture);

        return new Entity(staticModel, position, new float[] {0, 45, 0}, 1);
    }
}
