package com.example.openglexample.Z_GameEngine.gui;

import android.content.Context;
import android.renderscript.Matrix4f;

import com.example.openglexample.Z_GameEngine.Shaders.ShaderProgram;

/**
 * Created by Nitin Khurana on 09/11/18.
 */
public class GuiShader extends ShaderProgram{

    private static final String VERTEX_FILE = "shaders/gui_shader/vertexShader.glsl";
    private static final String FRAGMENT_FILE = "shaders/gui_shader/fragmentShader.glsl";

    private int location_transformationMatrix;

    public GuiShader(Context context) {
        super(context, VERTEX_FILE, FRAGMENT_FILE);
    }

    @Override
    protected void bindAttributes() {

        super.bindAttribute(0, "position");
    }

    @Override
    protected void getAllUniformLocations() {

        location_transformationMatrix = super.getUniformLocation("transformationMatrix");
    }

    public void loadTransformationMatrix(Matrix4f transformationMatrix) {
        super.loadMatrix(location_transformationMatrix, transformationMatrix);
    }
}
