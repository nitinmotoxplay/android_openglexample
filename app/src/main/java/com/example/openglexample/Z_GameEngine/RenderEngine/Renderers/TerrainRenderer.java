package com.example.openglexample.Z_GameEngine.RenderEngine.Renderers;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLES30;
import android.renderscript.Matrix4f;
import android.view.MotionEvent;

import com.example.openglexample.R;
import com.example.openglexample.Z_GameEngine.Entities.Camera;
import com.example.openglexample.Z_GameEngine.Entities.Light;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector3f;
import com.example.openglexample.Z_GameEngine.Shaders.TerrainShader;
import com.example.openglexample.Z_GameEngine.Terrain.Terrain;
import com.example.openglexample.Z_GameEngine.Terrain.TerrainTexture;
import com.example.openglexample.Z_GameEngine.Terrain.TerrainTexturePack;
import com.example.openglexample.Z_GameEngine.Toolbox.Maths;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nitin Khurana on 05/11/18.
 */
public class TerrainRenderer extends BaseRenderer<TerrainShader> {

    private List<Terrain> terrains = new ArrayList<>();
    private List<Terrain> terrainsToRender = new ArrayList<>();

    public TerrainRenderer(Context context, Camera camera, List<Light> lights) {
        super(context, camera, lights);
    }

    @Override
    public void onPrepare() {

        shader = new TerrainShader(context);

        TerrainTexture backgroundTexture = new TerrainTexture(loader.loadTexture(context, R.drawable.grassy));
        TerrainTexture rTexture = new TerrainTexture(loader.loadTexture(context, R.drawable.mud));
        TerrainTexture gTexture = new TerrainTexture(loader.loadTexture(context, R.drawable.grassflowers));
        TerrainTexture bTexture = new TerrainTexture(loader.loadTexture(context, R.drawable.path));

        TerrainTexturePack texturePack = new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);
        TerrainTexture blendMap = new TerrainTexture(loader.loadTexture(context, R.drawable.blendmap));

        terrains.add(new Terrain(context,-1, -1, loader, texturePack, blendMap, R.drawable.heightmap));
        terrains.add(new Terrain(context,0, -1, loader, texturePack, blendMap, R.drawable.heightmap));
//        terrains.add(new Terrain(context,-1, 0, loader, texturePack, blendMap, R.drawable.heightmap));
//        terrains.add(new Terrain(context,0, 0, loader, texturePack, blendMap, R.drawable.heightmap));
    }

    @Override
    public void onSurfaceChanged(Matrix4f projectionMatrix, Vector3f skyColor) {

        shader.start();

        shader.loadProjectionMatrix(projectionMatrix);
        shader.loadSkyColor(skyColor);
        shader.connectTextureUnits();

        shader.stop();
    }

    @Override
    public void onDrawFrame() {

        shader.start();
        shader.loadLights(lights);
        shader.loadViewMatrix(camera);

        for (Terrain terrain: terrains) {
            processTerrain(terrain);
        }

        render(terrainsToRender);

        shader.stop();

        terrainsToRender.clear();
    }

    @Override
    public void onInputReceived(float angle, MotionEvent event, int controlId) {

    }

    public List<Terrain> getTerrains() {
        return terrains;
    }

    private void prepareTerrain(Terrain terrain) {

        GLES30.glBindVertexArray(terrain.getModel().getVaoId());
        GLES30.glEnableVertexAttribArray(0);
        GLES30.glEnableVertexAttribArray(1);
        GLES30.glEnableVertexAttribArray(2);

        bindTextures(terrain);
        shader.loadShineVariables(1, 0);

    }

    private void bindTextures(Terrain terrain) {

        TerrainTexturePack terrainTexturePack = terrain.getTexturePack();

        GLES30.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES30.glBindTexture(GLES20.GL_TEXTURE_2D, terrainTexturePack.getBackgroundTexture().getTextureId());

        GLES30.glActiveTexture(GLES20.GL_TEXTURE1);
        GLES30.glBindTexture(GLES20.GL_TEXTURE_2D, terrainTexturePack.getrTexture().getTextureId());

        GLES30.glActiveTexture(GLES20.GL_TEXTURE2);
        GLES30.glBindTexture(GLES20.GL_TEXTURE_2D, terrainTexturePack.getgTexture().getTextureId());

        GLES30.glActiveTexture(GLES20.GL_TEXTURE3);
        GLES30.glBindTexture(GLES20.GL_TEXTURE_2D, terrainTexturePack.getbTexture().getTextureId());

        GLES30.glActiveTexture(GLES20.GL_TEXTURE4);
        GLES30.glBindTexture(GLES20.GL_TEXTURE_2D, terrain.getBlendMap().getTextureId());
    }

    private void loadModelMatrix(Terrain terrain ) {

        Matrix4f transformationMatrix = Maths.createTransformationMatrix(
                terrain.getX(), 0, terrain.getZ(),
                0, 0, 0, 1);

        shader.loadTransformationMatrix(transformationMatrix);
    }

    private void unbindTerrain() {

        GLES30.glDisableVertexAttribArray(0);
        GLES30.glDisableVertexAttribArray(1);
        GLES30.glDisableVertexAttribArray(2);
        GLES30.glBindVertexArray(0);
    }

    private void processTerrain(Terrain terrain) {
        terrainsToRender.add(terrain);
    }

    private void render(List<Terrain> terrains) {

        for (Terrain terrain: terrains) {

            prepareTerrain(terrain);
            loadModelMatrix(terrain);

            GLES30.glDrawElements(GLES30.GL_TRIANGLES, terrain.getModel().getVertexCount(),
                    GLES20.GL_UNSIGNED_INT, 0);

            unbindTerrain();
        }
    }

    public void loadFog(float density, float gradient) {
        shader.start();
        shader.loadFog(density, gradient);
        shader.stop();
    }
}
