package com.example.openglexample.Z_GameEngine.Models;

/**
 * Created by Nitin Khurana on 31/10/18.
 */
public class RawModel {

    private int vaoId;
    private int vertexCount;

    public RawModel(int vaoId, int vertexCount) {

        this.vaoId = vaoId;
        this.vertexCount = vertexCount;
    }

    public int getVaoId() {
        return vaoId;
    }

    public int getVertexCount() {
        return vertexCount;
    }

}
