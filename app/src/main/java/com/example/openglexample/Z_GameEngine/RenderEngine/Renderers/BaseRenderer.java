package com.example.openglexample.Z_GameEngine.RenderEngine.Renderers;

import android.content.Context;
import android.renderscript.Matrix4f;
import android.view.MotionEvent;

import com.example.openglexample.Z_GameEngine.Entities.Camera;
import com.example.openglexample.Z_GameEngine.Entities.Light;
import com.example.openglexample.Z_GameEngine.RenderEngine.Loader;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector3f;
import com.example.openglexample.Z_GameEngine.Shaders.ShaderProgram;

import java.util.List;

/**
 * Created by Nitin Khurana on 05/11/18.
 */
public abstract class BaseRenderer<Shader extends ShaderProgram> {

    protected Context context;

    protected List<Light> lights;
    protected Camera camera;

    protected Shader shader;
    protected Loader loader = new Loader();

    public BaseRenderer(Context context, Camera camera, List<Light> lights) {
        this.context = context;
        this.camera = camera;
        this.lights = lights;
    }

    public abstract void onPrepare();
    public abstract void onSurfaceChanged(Matrix4f projectionMatrix, Vector3f skyColor);
    public abstract void onDrawFrame();
    public abstract void onInputReceived(float angle, MotionEvent event, int controlId);

    public void cleanUp() {

        loader.cleanUp();
        shader.cleanUp();
    }
}
