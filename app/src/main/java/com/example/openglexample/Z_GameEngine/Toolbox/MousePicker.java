package com.example.openglexample.Z_GameEngine.Toolbox;

import android.renderscript.Matrix4f;
import android.view.MotionEvent;

import com.example.openglexample.Z_GameEngine.Entities.Camera;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector2f;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector3f;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector4f;

/**
 * Created by Nitin Khurana on 14/11/18.
 */
public class MousePicker {

    private Vector3f currentRay;

    private Matrix4f projectionMatrix;
    private Matrix4f viewMatrix;
    private Camera camera;

    private float x, y;
    private Vector2f screenSize;

    public MousePicker(Camera camera, Vector2f screenSize) {

        this.camera = camera;
        this.screenSize = screenSize;

        this.viewMatrix = Maths.createViewMatrix(camera);
    }

    public Vector3f getCurrentRay() {
        return currentRay;
    }

    public void setProjectionMatrix(Matrix4f projectionMatrix) {
        this.projectionMatrix = projectionMatrix;
    }

    public void update() {
        viewMatrix = Maths.createViewMatrix(camera);
        currentRay = calculateMouseRay(x, y);
    }

    public void onInputReceived(MotionEvent event) {

        this.x = event.getX();
        this.y = event.getY();
    }



    private Vector3f calculateMouseRay(float x, float y) {

        Vector2f normalizedCoords = getNormalizedDeviceCoords(x, y);
        Vector4f clipCoords = new Vector4f(normalizedCoords.x, normalizedCoords.y, -1f, 1f);
        Vector4f eyeCoords = toEyeCoords(clipCoords);
        Vector3f worldRay = toWorldCoords(eyeCoords);

        return worldRay;
    }



    private Vector2f getNormalizedDeviceCoords(float mouseX, float mouseY) {

        float x = (2f * mouseX) / screenSize.x -1;
        float y = (2f * mouseY) / screenSize.y -1;

        return new Vector2f(x, -y); // remove - minus from y
    }

    private Vector4f toEyeCoords(Vector4f clipCoords) {

        Matrix4f inverseMatrix = Maths.inverseMatrix(projectionMatrix);
        Vector4f eyeCoords = Maths.multiplyMatrix(clipCoords, inverseMatrix);

        return new Vector4f(eyeCoords.x, eyeCoords.y, -1f, 0f);
    }

    private Vector3f toWorldCoords(Vector4f eyeCoords) {

        Matrix4f inverseViewMatrix = Maths.inverseMatrix(viewMatrix);
        Vector4f rayWorld = Maths.multiplyMatrix(eyeCoords, inverseViewMatrix);

        Vector3f mouseRay = new Vector3f(rayWorld.x, rayWorld.y, rayWorld.z);
        mouseRay.normalize();

        return mouseRay;
    }
}
