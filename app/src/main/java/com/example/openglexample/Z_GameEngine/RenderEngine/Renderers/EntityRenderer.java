package com.example.openglexample.Z_GameEngine.RenderEngine.Renderers;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLES30;
import android.renderscript.Matrix4f;
import android.view.MotionEvent;

import com.example.openglexample.R;
import com.example.openglexample.Z_GameEngine.Entities.Camera;
import com.example.openglexample.Z_GameEngine.Entities.Entity;
import com.example.openglexample.Z_GameEngine.Entities.Light;
import com.example.openglexample.Z_GameEngine.Models.Player;
import com.example.openglexample.Z_GameEngine.Models.RawModel;
import com.example.openglexample.Z_GameEngine.Models.SampleModels;
import com.example.openglexample.Z_GameEngine.Models.TexturedModel;
import com.example.openglexample.Z_GameEngine.RenderEngine.MasterRenderer;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector3f;
import com.example.openglexample.Z_GameEngine.RenderEngine.OBJLoader.ModelData;
import com.example.openglexample.Z_GameEngine.RenderEngine.OBJLoader.OBJFileLoader;
import com.example.openglexample.Z_GameEngine.Shaders.StaticShader;
import com.example.openglexample.Z_GameEngine.Terrain.Terrain;
import com.example.openglexample.Z_GameEngine.Textures.ModelTexture;
import com.example.openglexample.Z_GameEngine.Toolbox.Maths;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Nitin Khurana on 05/11/18.
 */
public class EntityRenderer extends BaseRenderer<StaticShader> {

    private List<Entity> entities = new ArrayList<>();
    private TerrainRenderer terrainRenderer;
    private Map<TexturedModel, List<Entity>> entitiesMap = new HashMap<>();

    private Player player;

    public EntityRenderer(Context context, Camera camera, List<Light> lights, TerrainRenderer terrainRenderer) {
        super(context, camera, lights);
        this.terrainRenderer = terrainRenderer;
    }

    @Override
    public void onPrepare() {

        shader = new StaticShader(context);
        Random random = new Random();

        List<Entity> vividEntities = new ArrayList<>();
        vividEntities.add(SampleModels.createFlower(context, new float[]{0,0,0}, new float[] {0,0,0}, loader));
        vividEntities.add(SampleModels.createGrass(context, new float[]{0,0,0}, new float[] {0,0,0}, loader));
        vividEntities.add(SampleModels.createTree(context, new float[]{0,0,0}, new float[] {0,0,0}, loader));
        vividEntities.add(SampleModels.createLowPolyTree(context, new float[]{0,0,0}, new float[] {0,0,0}, loader));
        vividEntities.add(SampleModels.createStall(context, new float[]{0,0,0}, new float[] {0,0,0}, loader));
        vividEntities.add(SampleModels.createFern(context, new float[]{0,0,0}, new float[] {0,0,0}, loader));

        for (Terrain terrain : terrainRenderer.getTerrains()) {

            for (Entity entity : vividEntities) {

                for (int i= 0; i<50; i++) {

                    float[] rotation = new float[] {0, random.nextInt(360), 0};
                    float[] position = terrain.createRandomPosition();

                    Entity tempEntity = new Entity(entity);
                    tempEntity.setPosition(position);
                    tempEntity.setRotation(rotation);
                    tempEntity.setTextureIndex(random.nextInt(4));

                    entities.add(tempEntity);
                }
            }
        }

        Entity referenceLampModel = SampleModels.createLamp(context, new float[3], new float[3], loader);
        for (int i=0; i<3; i++) {

            float[] rotation = new float[] {0, random.nextInt(360), 0};
            float[] position = new float[] {0, 0, i * 30 - 400};
            Vector3f lightColor = new Vector3f(1, 1, 1);
            switch (i) {
                case 0: lightColor = new Vector3f(1, 0, 0); break;
                case 1: lightColor = new Vector3f(0, 1, 0); break;
                case 2: lightColor = new Vector3f(0, 0, 5); break;
            }

            Entity lampModel = new Entity(referenceLampModel.getModel(), position, rotation, 1);
            Light light = new Light(new Vector3f(position[0], position[1] + 14.2f, position[2]),
                    lightColor, new Vector3f(1, 0.01f, 0.002f));

            entities.add(lampModel);
            lights.add(light);
        }

        ModelData playerModelData = OBJFileLoader.loadOBJ(context, "Models/person.obj");
        RawModel playerModel = loader.loadToVao(playerModelData.getVertices(), playerModelData.getTextureCoords(), playerModelData.getNormals(), playerModelData.getIndices());
        ModelTexture playerTexture = new ModelTexture(loader.loadTexture(context, R.drawable.playertexture));
        TexturedModel playerTextureModel = new TexturedModel(playerModel, playerTexture);
        player = new Player(playerTextureModel, new float[]{0, 0, -400}, new float[] {0, -90, 0}, 0.5f);

        entities.add(player);

        camera.setPlayer(player);
    }

    @Override
    public void onSurfaceChanged(Matrix4f projectionMatrix, Vector3f skyColor) {

        shader.start();

        shader.loadProjectionMatrix(projectionMatrix);
        shader.loadSkyColor(skyColor);

        shader.stop();
    }

    @Override
    public void onDrawFrame() {

        shader.start();
        shader.loadLights(lights);
        shader.loadViewMatrix(camera);

        for(Entity entity: entities) {
            processEntity(entity);
        }

        render(entitiesMap);

        Terrain playerTerrain = terrainRenderer.getTerrains().get(0);
        for(Terrain terrain : terrainRenderer.getTerrains()) {
            if(player.getPosition()[0] >= terrain.getX() && player.getPosition()[0] < (terrain.getX() + Terrain.SIZE) &&
                    player.getPosition()[2] >= terrain.getZ() && player.getPosition()[2] < (terrain.getZ() + Terrain.SIZE) )
            {
                playerTerrain = terrain;
                break;
            }
        }

        player.update(playerTerrain);

        shader.stop();

        entitiesMap.clear();
    }

    @Override
    public void onInputReceived(float angle, MotionEvent event, int controlId) {

        if(event != null)
           player.checkInputs(controlId, event);
    }

    @Override
    public void cleanUp() {
        super.cleanUp();
    }

    private void prepareTexturedModel(TexturedModel model) {

        GLES30.glBindVertexArray(model.getRawModel().getVaoId());
        GLES30.glEnableVertexAttribArray(0);
        GLES30.glEnableVertexAttribArray(1);
        GLES30.glEnableVertexAttribArray(2);

        ModelTexture texture = model.getTexture();
        if (texture.isHasTransparency()) {
            MasterRenderer.disableCulling();
        }

        shader.loadShineVariables(texture.getShineDamper(), texture.getReflectivity());
        shader.loadFakeLighting(texture.isUseFakeLighting());
        shader.loadNumberOfRows(texture.getNumberOfRows());

        GLES30.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES30.glBindTexture(GLES20.GL_TEXTURE_2D, texture.getTextureId());
    }

    private void prepareInstance(Entity entity) {

        Matrix4f transformationMatrix = Maths.createTransformationMatrix(
                entity.getPosition()[0], entity.getPosition()[1], entity.getPosition()[2],
                entity.getRotation()[0], entity.getRotation()[1], entity.getRotation()[2],
                entity.getScale());

        shader.loadTransformationMatrix(transformationMatrix);
        shader.loadOffset(entity.getTextureXOffset(), entity.getTextureYOffset());
    }

    private void unbindTexturedModel() {
        MasterRenderer.enableCulling();

        GLES30.glDisableVertexAttribArray(0);
        GLES30.glDisableVertexAttribArray(1);
        GLES30.glDisableVertexAttribArray(2);
        GLES30.glBindVertexArray(0);
    }

    private void processEntity(Entity entity) {

        TexturedModel model = entity.getModel();
        List<Entity> batch = entitiesMap.get(model);

        if (batch != null) {
            // it's a new batch
            batch.add(entity);
        }
        else {
            List<Entity> newBatch = new ArrayList<>();
            newBatch.add(entity);
            entitiesMap.put(model, newBatch);
        }
    }

    private void render(Map<TexturedModel, List<Entity>> entities) {

        for(TexturedModel model:entities.keySet()) {

            prepareTexturedModel(model);
            List<Entity> batch = entities.get(model);

            for (Entity entity: batch) {
                prepareInstance(entity);

                GLES30.glDrawElements(GLES30.GL_TRIANGLES, model.getRawModel().getVertexCount(), GLES20.GL_UNSIGNED_INT, 0);

            }

            unbindTexturedModel();
        }
    }

    public void loadFog(float density, float gradient) {
        shader.start();
        shader.loadFog(density, gradient);
        shader.stop();
    }
}
