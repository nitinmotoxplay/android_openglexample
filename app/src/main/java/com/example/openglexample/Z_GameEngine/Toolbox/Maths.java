package com.example.openglexample.Z_GameEngine.Toolbox;

import android.opengl.Matrix;
import android.renderscript.Matrix4f;

import com.example.openglexample.Z_GameEngine.Entities.Camera;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector2f;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector3f;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector4f;

/**
 * Created by Nitin Khurana on 03/11/18.
 */
public class Maths {

    public static Matrix4f createTransformationMatrix(
            float x, float y, float z,
            float rx, float ry, float rz,
            float scale) {

        float[] matrix = new float[16];
        Matrix.setIdentityM(matrix, 0);
        Matrix.translateM(matrix, 0, x, y, z);
        Matrix.rotateM(matrix, 0, rx, 1, 0, 0);
        Matrix.rotateM(matrix, 0, ry, 0, 1, 0);
        Matrix.rotateM(matrix, 0, rz, 0, 0, 1);
        Matrix.scaleM(matrix, 0, scale, scale, scale);

        return new Matrix4f(matrix);
    }

    public static Matrix4f createTransformationMatrix(Vector2f position, Vector2f scale) {

        float[] matrix = new float[16];
        Matrix.setIdentityM(matrix, 0);
        Matrix.translateM(matrix, 0, position.x, position.y, 0);
        Matrix.scaleM(matrix, 0, scale.x, scale.y, 1);
        return  new Matrix4f(matrix);
    }


    public static Matrix4f createViewMatrix(Camera camera) {

        float[] matrix = new float[16];
        Matrix.setIdentityM(matrix, 0);
        Matrix.rotateM(matrix, 0, camera.getPitch(), 1, 0, 0);
        Matrix.rotateM(matrix, 0, camera.getYaw(), 0, 1, 0);
        Matrix.rotateM(matrix, 0, camera.getRoll(), 0, 0, 1);
        Matrix.translateM(matrix, 0, -camera.getPosition()[0],-camera.getPosition()[1],-camera.getPosition()[2] );

        return new Matrix4f(matrix);
    }

    public static float barryCentric(Vector3f p1, Vector3f p2, Vector3f p3, Vector2f pos) {
        float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
        float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
        float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
        float l3 = 1.0f - l1 - l2;
        return l1 * p1.y + l2 * p2.y + l3 * p3.y;
    }

    public static Vector4f multiplyMatrix(Vector4f vector, Matrix4f matrix) {

        float[] input = new float[] {vector.x, vector.y, vector.z, vector.w};
        float[] output = new float[4];

        float[] matrixArray = matrix.getArray();

        Matrix.multiplyMV(output, 0, matrixArray, 0, input, 0);

        return new Vector4f(output);
    }

    public static Matrix4f inverseMatrix(Matrix4f matrix) {

        float[] inputArray = matrix.getArray();
        float[] outputArray = new float[16];

        Matrix.invertM(outputArray, 0, inputArray, 0);

        return new Matrix4f(outputArray);
    }
}
