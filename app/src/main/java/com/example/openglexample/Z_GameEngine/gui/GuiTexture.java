package com.example.openglexample.Z_GameEngine.gui;

import com.example.openglexample.Z_GameEngine.MathClasses.Vector2f;

/**
 * Created by Nitin Khurana on 09/11/18.
 */
public class GuiTexture {

    private int texture;
    private Vector2f scale;
    private Vector2f position;

    public GuiTexture(int texture, Vector2f position, Vector2f scale) {
        this.texture = texture;
        this.scale = scale;
        this.position = position;
    }

    public int getTexture() {
        return texture;
    }

    public Vector2f getScale() {
        return scale;
    }

    public Vector2f getPosition() {
        return position;
    }
}
