package com.example.openglexample.Z_GameEngine.Entities;

/**
 * Created by Nitin Khurana on 11/11/18.
 */
public class LightEntity {

    private Entity entity;
    private Light light;

    public LightEntity(Entity entity, Light light) {
        this.entity = entity;
        this.light = light;
    }

    public Entity getEntity() {
        return entity;
    }

    public Light getLight() {
        return light;
    }
}
