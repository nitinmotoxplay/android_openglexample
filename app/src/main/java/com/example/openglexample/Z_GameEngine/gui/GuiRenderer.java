package com.example.openglexample.Z_GameEngine.gui;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLES30;
import android.renderscript.Matrix4f;
import android.view.MotionEvent;

import com.example.openglexample.R;
import com.example.openglexample.Z_GameEngine.Entities.Camera;
import com.example.openglexample.Z_GameEngine.Entities.Light;
import com.example.openglexample.Z_GameEngine.Models.RawModel;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector2f;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector3f;
import com.example.openglexample.Z_GameEngine.RenderEngine.Renderers.BaseRenderer;
import com.example.openglexample.Z_GameEngine.Toolbox.Maths;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nitin Khurana on 09/11/18.
 */
public class GuiRenderer extends BaseRenderer<GuiShader>{

    private RawModel quad;

    private List<GuiTexture> guis = new ArrayList<>();

    public GuiRenderer(Context context, Camera camera) {
        super(context, camera, new ArrayList<Light>());
    }


    private void render() {

        shader.start();

        GLES30.glBindVertexArray(quad.getVaoId());
        GLES30.glEnableVertexAttribArray(0);
        GLES30.glEnable(GLES30.GL_BLEND);
        GLES30.glDisable(GLES30.GL_DEPTH_TEST);
        GLES30.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        for(GuiTexture gui : guis) {
            GLES30.glActiveTexture(GLES30.GL_TEXTURE0);
            GLES30.glBindTexture(GLES20.GL_TEXTURE_2D, gui.getTexture());
            shader.loadTransformationMatrix(Maths.createTransformationMatrix(gui.getPosition(), gui.getScale()));
            GLES30.glDrawArrays(GLES30.GL_TRIANGLE_STRIP, 0, quad.getVertexCount());
        }

        GLES30.glDisable(GLES30.GL_BLEND);
        GLES30.glEnable(GLES30.GL_DEPTH_TEST);
        GLES30.glDisableVertexAttribArray(0);
        GLES30.glBindVertexArray(0);

        shader.stop();
    }

    @Override
    public void onPrepare() {

        quad = loader.loadToVao(new float[] { -1, 1, -1, -1, 1, 1, 1, -1}, 2);
        shader = new GuiShader(context);

        GuiTexture gui = new GuiTexture(loader.loadTexture(context, R.drawable.fern),
                new Vector2f(0.5f, 0.5f), new Vector2f(0.25f, 0.25f));
//        guis.add(gui);

    }

    @Override
    public void onSurfaceChanged(Matrix4f projectionMatrix, Vector3f skyColor) {

    }

    @Override
    public void onDrawFrame() {

        render();
    }

    @Override
    public void onInputReceived(float angle, MotionEvent event, int controlId) {

    }
}
