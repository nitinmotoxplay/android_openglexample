package com.example.openglexample.Z_GameEngine.Entities;

import com.example.openglexample.Z_GameEngine.Models.TexturedModel;

/**
 * Created by Nitin Khurana on 03/11/18.
 */
public class Entity {

    private TexturedModel model;
    private float[] position;
    private float[] rotation;
    private float scale;

    private int textureIndex = 0;

    public Entity(TexturedModel model, float[] position, float[] rotation, float scale) {
        this.model = model;
        this.position = position;
        this.rotation = rotation;
        this.scale = scale;
    }

    public Entity(Entity entity) {
        this.model = entity.getModel();
        this.position = entity.getPosition();
        this.rotation = entity.getRotation();
        this.scale = entity.getScale();
    }

    protected void increasePosition(float dx, float dy, float dz) {

        position[0] += dx;
        position[1] += dy;
        position[2] += dz;
    }

    protected void increaseRoation(float dx, float dy, float dz) {

        rotation[0] += dx;
        rotation[1] += dy;
        rotation[2] += dz;
    }

    public float getTextureXOffset(){

        int column = textureIndex % model.getTexture().getNumberOfRows();
        return (float) column / (float) model.getTexture().getNumberOfRows();
    }

    public float getTextureYOffset(){
        int column = textureIndex / model.getTexture().getNumberOfRows();
        return (float) column / (float) model.getTexture().getNumberOfRows();
    }

    public TexturedModel getModel() {
        return model;
    }

    public void setModel(TexturedModel model) {
        this.model = model;
    }

    public float[] getPosition() {
        return position;
    }

    public void setPosition(float[] position) {
        this.position = position;
    }

    public float[] getRotation() {
        return rotation;
    }

    public void setRotation(float[] rotation) {
        this.rotation = rotation;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public void setTextureIndex(int textureIndex) {
        this.textureIndex = textureIndex;
    }
}
