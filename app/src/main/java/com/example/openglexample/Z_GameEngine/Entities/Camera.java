package com.example.openglexample.Z_GameEngine.Entities;

import com.example.openglexample.R;
import com.example.openglexample.Z_GameEngine.Models.Player;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector3f;

/**
 * Created by Nitin Khurana on 04/11/18.
 */
public class Camera {

    private float distanceFromPlayer = 16f;
    private float angleAroundPlayer = 0;

    private float[] position;
    private float pitch;
    private float yaw;
    private float roll;

    private Player player;

    public Camera(Vector3f position) {
        this.position = new float[] {position.x, position.y, position.z};
    }

    public float[] getPosition() {
        return position;
    }

    public float getPitch() {
        return pitch;
    }

    public float getYaw() {
        return yaw;
    }

    public float getRoll() {
        return roll;
    }

    public void setPosition(float[] position) {
        this.position = position;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public void setRoll(float roll) {
        this.roll = roll;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void update() {

        float horizontalDistance = calculateHorizontalDistance();
        float verticalDistance = calculateVerticalDistance();
        if(player != null)
            calculateCameraPosition(horizontalDistance, verticalDistance);
        setYaw(180 - (angleAroundPlayer + player.getRotation()[1]));
    }

    public void checkInputs(float angle, int button) {

        if(button == R.id.zoom) {

            float zoomLevel = angle * 0.1f;
            distanceFromPlayer -= zoomLevel;
        }
        else if(button == R.id.pitch) {

            float pitchChage = angle * 0.5f;
            pitch -= pitchChage;
        }
        else{

            float angleChange = angle * 0.5f;
            angleAroundPlayer += angleChange;
        }

    }

    private float calculateHorizontalDistance() {
        return (float) (distanceFromPlayer * Math.cos(Math.toRadians(pitch)));
    }

    private float calculateVerticalDistance() {
        return (float) (distanceFromPlayer * Math.sin(Math.toRadians(pitch)));
    }

    private void calculateCameraPosition(float horizontalDistance, float verticalDistance) {

        float theta = player.getRotation()[1] + angleAroundPlayer;
        float offsetX = (float) (horizontalDistance * Math.sin(Math.toRadians(theta)));
        float offsetZ = (float) (horizontalDistance * Math.cos(Math.toRadians(theta)));

        position[0] = player.getPosition()[0] - offsetX;
        position[1] = player.getPosition()[1] + verticalDistance;
        position[2] = player.getPosition()[2] - offsetZ;

    }
}
