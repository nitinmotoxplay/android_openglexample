package com.example.openglexample.Z_GameEngine.Water;

import android.content.Context;
import android.opengl.GLES30;
import android.renderscript.Matrix4f;
import android.view.MotionEvent;

import com.example.openglexample.Z_GameEngine.Entities.Camera;
import com.example.openglexample.Z_GameEngine.Entities.Light;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector3f;
import com.example.openglexample.Z_GameEngine.Models.RawModel;
import com.example.openglexample.Z_GameEngine.RenderEngine.Renderers.BaseRenderer;
import com.example.openglexample.Z_GameEngine.Toolbox.Maths;

import java.util.ArrayList;
import java.util.List;

public class WaterRenderer extends BaseRenderer<WaterShader>{

	private RawModel quad;
	private List<WaterTile> waters = new ArrayList<>();

	public WaterRenderer(Context context, Camera camera, List<Light> lights) {
		super(context, camera, lights);
 	}

	@Override
	public void onPrepare() {

		shader = new WaterShader(context);

		// create the water quad
		// Just x and z vertex positions here, y is set to 0 in v.shader
		float[] vertices = { -1, -1, -1, 1, 1, -1, 1, -1, -1, 1, 1, 1 };
		quad = loader.loadToVao(vertices, 2);

		waters.add(new WaterTile(0, -400, 2f));
	}

	@Override
	public void onSurfaceChanged(Matrix4f projectionMatrix, Vector3f skyColor) {

		shader.start();
		shader.loadProjectionMatrix(projectionMatrix);
		shader.stop();
	}

	@Override
	public void onDrawFrame() {

		shader.start();
		shader.loadViewMatrix(camera);
		GLES30.glBindVertexArray(quad.getVaoId());
		GLES30.glEnableVertexAttribArray(0);

		for (WaterTile tile : waters) {

			Matrix4f modelMatrix = Maths.createTransformationMatrix(
					tile.getX(), tile.getHeight(), tile.getZ(),
					0f, 0f, 0f,
					WaterTile.TILE_SIZE);

			shader.loadModelMatrix(modelMatrix);
			GLES30.glDrawArrays(GLES30.GL_TRIANGLES, 0, quad.getVertexCount());
		}

		GLES30.glDisableVertexAttribArray(0);
		GLES30.glBindVertexArray(0);
		shader.stop();

	}

	@Override
	public void onInputReceived(float angle, MotionEvent event, int controlId) {

	}


}
