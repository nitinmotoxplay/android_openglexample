package com.example.openglexample.Z_GameEngine.Terrain;

/**
 * Created by Nitin Khurana on 06/11/18.
 */
public class TerrainTexture {

    private int textureId;

    public TerrainTexture(int textureId) {
        this.textureId = textureId;
    }

    public int getTextureId() {
        return textureId;
    }
}
