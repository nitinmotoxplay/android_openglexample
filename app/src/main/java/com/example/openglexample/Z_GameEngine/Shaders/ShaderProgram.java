package com.example.openglexample.Z_GameEngine.Shaders;

import android.content.Context;
import android.content.res.AssetManager;
import android.opengl.GLES20;
import android.opengl.GLES30;
import android.renderscript.Matrix4f;

import com.example.openglexample.Z_GameEngine.MathClasses.Vector2f;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector3f;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Nitin Khurana on 31/10/18.
 */
public abstract class ShaderProgram {

    private int programId;
    private int vertexShaderId;
    private int fragmentShaderId;

    public ShaderProgram(Context context, String vertexFile, String fragmentFile) {

        vertexShaderId = loadShader(context, vertexFile, GLES20.GL_VERTEX_SHADER);
        fragmentShaderId = loadShader(context, fragmentFile, GLES20.GL_FRAGMENT_SHADER);

        programId = GLES20.glCreateProgram();
        GLES20.glAttachShader(programId, vertexShaderId);
        GLES20.glAttachShader(programId, fragmentShaderId);

        bindAttributes();

        GLES20.glLinkProgram(programId);

        int[] linkStatus = new int[1];
        GLES20.glGetProgramiv(programId, GLES20.GL_LINK_STATUS, linkStatus, 0);
        if (linkStatus[0] != GLES20.GL_TRUE) {
            String error = GLES20.glGetProgramInfoLog(programId);
            GLES20.glDeleteProgram(programId);
            throw new RuntimeException("Could not link program: " + error);
        }

        GLES20.glValidateProgram(programId);

        getAllUniformLocations();
    }

    public void start() {

        GLES20.glUseProgram(programId);
    }

    public void stop() {

        GLES20.glUseProgram(0);
    }

    public void cleanUp() {
        stop();
        GLES20.glDetachShader(programId, vertexShaderId);
        GLES20.glDetachShader(programId, fragmentShaderId);
        GLES20.glDeleteShader(vertexShaderId);
        GLES20.glDeleteShader(fragmentShaderId);
        GLES20.glDeleteShader(programId);

    }

    protected abstract void bindAttributes();

    protected void bindAttribute(int attribute, String variableName) {
        GLES20.glBindAttribLocation(programId, attribute, variableName);
    }


    protected abstract void getAllUniformLocations();

    protected  int getUniformLocation(String uniformName) {
        return GLES30.glGetUniformLocation(programId, uniformName);
    }

    protected void loadFloat(int location, float value) {
        GLES30.glUniform1f(location, value);
    }

    protected void loadInt(int location, int i) {
        GLES30.glUniform1i(location, i);
    }

    protected void loadVector(int location, Vector3f vector) {
        GLES30.glUniform3f(location, vector.x, vector.y, vector.z);
    }

    protected void loadVector(int location, Vector2f vector) {
        GLES30.glUniform2f(location, vector.x, vector.y);
    }

    protected void loadBoolan(int location, boolean value) {
        GLES30.glUniform1f(location, value? 1: 0);
    }

    protected void loadMatrix(int location, Matrix4f matrix) {
        GLES30.glUniformMatrix4fv(location, 1, false, matrix.getArray(), 0);
    }


    private static int loadShader(Context context, String file, int type) {

        StringBuilder shaderSource = new StringBuilder();
        try {
            AssetManager assetManager = context.getAssets();

            InputStream inputStream = assetManager.open(file);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String inputLine;
            while ((inputLine = bufferedReader.readLine()) != null)
            {
                shaderSource.append(inputLine);
                shaderSource.append('\n');
            }

            inputStream.close();
            bufferedReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        int shaderId = GLES20.glCreateShader(type);

        GLES20.glShaderSource(shaderId, shaderSource.toString());
        GLES20.glCompileShader(shaderId);

        int[] compiled = new int[1];
        GLES20.glGetShaderiv(shaderId, GLES20.GL_COMPILE_STATUS, compiled, 0);
        if (compiled[0] == 0) {
            String message = GLES30.glGetShaderInfoLog(shaderId);
            GLES20.glDeleteShader(shaderId);
            throw new RuntimeException("Could not compile program: "
                    + message + " | " + shaderSource);
        }

        return shaderId;
    }



}
