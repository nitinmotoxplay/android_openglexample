package com.example.openglexample.Z_GameEngine.Terrain;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.openglexample.Z_GameEngine.Models.RawModel;
import com.example.openglexample.Z_GameEngine.RenderEngine.Loader;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector2f;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector3f;
import com.example.openglexample.Z_GameEngine.Toolbox.Maths;

import java.util.Random;

/**
 * Created by Nitin Khurana on 05/11/18.
 */
public class Terrain {

    public static final float SIZE = 800;
    private static final float MAX_HEIGHT = 40;
    private static final float MAX_PIXEL_COLOR = 256 * 256 * 256;

    private float x;
    private float z;
    private RawModel model;
    private TerrainTexturePack texturePack;
    private TerrainTexture blendMap;

    private float[][] heights;

    public Terrain(Context context, int gridX, int gridZ, Loader loader, TerrainTexturePack texturePack, TerrainTexture blendMap, int heightMap) {

        this.texturePack = texturePack;
        this.blendMap = blendMap;

        this.x = gridX * SIZE;
        this.z = gridZ * SIZE;

        this.model = generateTerrain(context, loader, heightMap);
    }

    private RawModel generateTerrain(Context context, Loader loader, int heightMap){

        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), heightMap);

        int VERTEX_COUNT = bitmap.getHeight();
        heights = new float[VERTEX_COUNT][VERTEX_COUNT];

        int count = VERTEX_COUNT * VERTEX_COUNT;
        float[] vertices = new float[count * 3];
        float[] normals = new float[count * 3];
        float[] textureCoords = new float[count*2];
        int[] indices = new int[6*(VERTEX_COUNT-1)*(VERTEX_COUNT-1)];
        int vertexPointer = 0;
        for(int i=0;i<VERTEX_COUNT;i++){
            for(int j=0;j<VERTEX_COUNT;j++){
                vertices[vertexPointer*3] = (float)j/((float)VERTEX_COUNT - 1) * SIZE;
                float height  = getHeight(j, i, bitmap);
                heights[j][i] = height;
                vertices[vertexPointer*3+1] = height;
                vertices[vertexPointer*3+2] = (float)i/((float)VERTEX_COUNT - 1) * SIZE;
                Vector3f normal = calculateNormal(j, i, bitmap);
                normals[vertexPointer*3] = normal.x;
                normals[vertexPointer*3+1] = normal.y;
                normals[vertexPointer*3+2] = normal.z;
                textureCoords[vertexPointer*2] = (float)j/((float)VERTEX_COUNT - 1);
                textureCoords[vertexPointer*2+1] = (float)i/((float)VERTEX_COUNT - 1);
                vertexPointer++;
            }
        }
        int pointer = 0;
        for(int gz=0;gz<VERTEX_COUNT-1;gz++){
            for(int gx=0;gx<VERTEX_COUNT-1;gx++){
                int topLeft = (gz*VERTEX_COUNT)+gx;
                int topRight = topLeft + 1;
                int bottomLeft = ((gz+1)*VERTEX_COUNT)+gx;
                int bottomRight = bottomLeft + 1;
                indices[pointer++] = topLeft;
                indices[pointer++] = bottomLeft;
                indices[pointer++] = topRight;
                indices[pointer++] = topRight;
                indices[pointer++] = bottomLeft;
                indices[pointer++] = bottomRight;
            }
        }
        return loader.loadToVao(vertices, textureCoords, normals, indices);
    }

    public float getX() {
        return x;
    }

    public float getZ() {
        return z;
    }

    public RawModel getModel() {
        return model;
    }

    public TerrainTexturePack getTexturePack() {
        return texturePack;
    }

    public TerrainTexture getBlendMap() {
        return blendMap;
    }

    private Vector3f calculateNormal(int x, int z, Bitmap image) {

        float heightL = getHeight(x-1, z, image);
        float heightR = getHeight(x+1, z, image);
        float heightD = getHeight(x, z-1, image);
        float heightU = getHeight(x, z+1, image);

        Vector3f normal = new Vector3f(heightL-heightR, 2f, heightD-heightU);
        normal.normalize();
        return normal;

//        return new Vector3f(0, 1, 0);
    }

    private float getHeight(int x, int z, Bitmap image) {

        if(x<0 || x>=image.getHeight() || z<0 || z>=image.getHeight()) {
            return 0;
        }

        float height = image.getPixel(x, z);
        height += MAX_PIXEL_COLOR/2f;
        height /= MAX_PIXEL_COLOR/2f;
        height *= MAX_HEIGHT;

        return height;
    }

    public float[] createRandomPosition() {

        float x, z;
        Random random = new Random();

        x = random.nextInt((int) SIZE - 40) + getX() + 20;
        z = random.nextInt((int) SIZE) + getZ();

        return new float[] {x, getHeightOfTerrain(x, z), z};
    }

    public float getHeightOfTerrain(float worldX, float worldZ) {

        float terrainX = worldX - this.x;
        float terrainZ = worldZ - this.z;

        float gridSquareSize = SIZE / ((float) (heights.length - 1));
        int gridX = (int)  Math.floor(terrainX / gridSquareSize);
        int gridZ = (int)  Math.floor(terrainZ / gridSquareSize);

        if(gridX >= heights.length -1 || gridZ >= heights.length - 1 || gridX < 0 || gridZ < 0){
            return 0;
        }

        float xCoord = (terrainX % gridSquareSize) / gridSquareSize;
        float zCoord = (terrainZ % gridSquareSize) / gridSquareSize;

        float answer;
        if (xCoord < (1-zCoord)) {
            answer = Maths
                    .barryCentric(new Vector3f(0, heights[gridX][gridZ], 0), new Vector3f(1,
                            heights[gridX + 1][gridZ], 0), new Vector3f(0,
                            heights[gridX][gridZ + 1], 1), new Vector2f(xCoord, zCoord));
        } else {
            answer = Maths
                    .barryCentric(new Vector3f(1, heights[gridX + 1][gridZ], 0), new Vector3f(1,
                            heights[gridX + 1][gridZ + 1], 1), new Vector3f(0,
                            heights[gridX][gridZ + 1], 1), new Vector2f(xCoord, zCoord));
        }

        return answer;
    }

}
