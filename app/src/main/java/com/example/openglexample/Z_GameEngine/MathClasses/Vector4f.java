package com.example.openglexample.Z_GameEngine.MathClasses;

import com.example.openglexample.Helper.HelperFunctions;

/**
 * Created by Nitin Khurana on 14/11/18.
 */
public class Vector4f {

    public float x, y, z, w;

    public Vector4f(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public Vector4f(float[] array) {
        x = array[0];
        y = array[1];
        z = array[2];
        w = array[3];
    }

    public void print() {
        HelperFunctions.showLog("x : " + x + " y : " + y + " z : " + z + " w : " + w);
    }
}
