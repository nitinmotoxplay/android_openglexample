package com.example.openglexample.Z_GameEngine.Skybox;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLES30;
import android.renderscript.Matrix4f;
import android.view.MotionEvent;

import com.example.openglexample.R;
import com.example.openglexample.Z_GameEngine.Entities.Camera;
import com.example.openglexample.Z_GameEngine.Entities.Light;
import com.example.openglexample.Z_GameEngine.Models.RawModel;
import com.example.openglexample.Z_GameEngine.RenderEngine.MasterRenderer;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector3f;
import com.example.openglexample.Z_GameEngine.RenderEngine.Renderers.BaseRenderer;

import java.util.List;

/**
 * Created by Nitin Khurana on 13/11/18.
 */
public class SkyBoxRenderer extends BaseRenderer<SkyBoxShader>{

    private static final float SIZE = 500f;
    private static float DAY_NIGHT_SPEED = 100; // 10-100;
    private static final int GAME_START_TIME = 11000;
    private static boolean dynamicFog = false;

    private float[] VERTICES = new float[36];
    private int[] TEXTURE_FILES = {R.drawable.right, R.drawable.left, R.drawable.top, R.drawable.bottom, R.drawable.back, R.drawable.front};
    private int[] TEXTURE_FILES_NIGHT = {R.drawable.night_right, R.drawable.night_left, R.drawable.night_top, R.drawable.night_bottom, R.drawable.night_back, R.drawable.night_front};

    private RawModel cube;
    private int texture;
    private int textureNight;

    private Vector3f fogColor;
    private MasterRenderer masterRenderer;
    private float time = GAME_START_TIME;

    public SkyBoxRenderer(Context context, Camera camera, List<Light> lights, MasterRenderer masterRenderer) {
        super(context, camera, lights);

        this.masterRenderer = masterRenderer;
        createCubeVertices();
    }

    @Override
    public void onPrepare() {

        shader = new SkyBoxShader(context);
        texture = loader.loadCubeMap(context, TEXTURE_FILES);
        textureNight = loader.loadCubeMap(context, TEXTURE_FILES_NIGHT);

        cube = loader.loadToVao(VERTICES, 3);
    }

    @Override
    public void onSurfaceChanged(Matrix4f projectionMatrix, Vector3f skyColor) {
        this.fogColor = skyColor;

        shader.start();

        shader.loadProjectionMatrix(projectionMatrix);
        shader.connectTextureUnits();

        shader.stop();
    }

    @Override
    public void onDrawFrame() {

        shader.start();

        shader.loadViewMatrix(camera);
        shader.loadFogColor(fogColor);

        GLES30.glBindVertexArray(cube.getVaoId());
        GLES30.glEnableVertexAttribArray(0);

        bindTextures();

        GLES30.glDrawArrays(GLES20.GL_TRIANGLES, 0, cube.getVertexCount());

        GLES30.glDisableVertexAttribArray(0);
        GLES30.glBindVertexArray(0);

        shader.stop();
    }

    private void bindTextures(){
        time += MasterRenderer.getFrameTimeSeconds() * DAY_NIGHT_SPEED;
        time %= 24000;
        int texture1;
        int texture2;

        Light sun = lights.get(0);

        // not foggy
        float gradient = 1.0f;
        float density = 0.0f;

        float blendFactor;
        float sunlight = 0.0f;
        if(time >= 0 && time < 5000){
            texture1 = textureNight;
            texture2 = textureNight;
            blendFactor = (time - 0)/(5000 - 0);

            sunlight = 0.0f;
            // fog calc
            density -= 0.0035/5000f;
            gradient += 3.5 / 5000f;

        }else if(time >= 5000 && time < 8000){
            texture1 = textureNight;
            texture2 = texture;
            blendFactor = (time - 5000)/(8000 - 5000);
            sunlight = (time - 5000)/10000f;

            // fog calc
            density += 0.0035f / 3000f;
            gradient -= 0.00116f;

        }else if(time >= 8000 && time < 15000){
            texture1 = texture;
            texture2 = texture;
            blendFactor = (time - 8000)/(17000 - 8000);
            sunlight = (time - 5000)/10000f;
            if(sunlight > 1.0f)
                sunlight = 1.0f;

            // fog calc
            if(time < 11000){
                density -= 0.0035/3000f;
                gradient += 0.00116f;

            }else{
                density -= 0.0035/4000f;
                gradient -= 0.001f;
            }

        }else if(time >= 15000 && time < 19000){
            texture1 = texture;
            texture2 = texture;
            blendFactor = (time - 15000)/(19000 - 15000);
            sunlight = (25000 - time)/10000f;

            // fog calc
            density += 0.0035/4000f;
            gradient += 0.001f;

        }else{
            texture1 = texture;
            texture2 = textureNight;
            blendFactor = (time - 19000)/(24000 - 19000);
            sunlight = (25000 - time)/10000f;

            // fog calc
            density += 0.0035/5000f;
            gradient -= 3.5 / 5000f;

        }

        sun.setColor(new Vector3f(sunlight/3.0f, sunlight/3.0f, sunlight/3.0f));

//        HelperFunctions.showLog("G " + gradient + " D " + density);

        if(dynamicFog)
            masterRenderer.setFog(density, gradient * 4.0f);


        GLES30.glActiveTexture(GLES30.GL_TEXTURE0);
        GLES30.glBindTexture(GLES30.GL_TEXTURE_CUBE_MAP, texture1);
        GLES30.glActiveTexture(GLES30.GL_TEXTURE1);
        GLES30.glBindTexture(GLES30.GL_TEXTURE_CUBE_MAP, texture2);

        shader.loadBlendFactor(blendFactor);
    }


    @Override
    public void onInputReceived(float angle, MotionEvent event, int controlId) {

    }

    private void createCubeVertices() {

        VERTICES = new float[]{

                -SIZE,  SIZE, -SIZE,
                -SIZE, -SIZE, -SIZE,
                SIZE, -SIZE, -SIZE,
                SIZE, -SIZE, -SIZE,
                SIZE,  SIZE, -SIZE,
                -SIZE,  SIZE, -SIZE,

                -SIZE, -SIZE,  SIZE,
                -SIZE, -SIZE, -SIZE,
                -SIZE,  SIZE, -SIZE,
                -SIZE,  SIZE, -SIZE,
                -SIZE,  SIZE,  SIZE,
                -SIZE, -SIZE,  SIZE,

                SIZE, -SIZE, -SIZE,
                SIZE, -SIZE,  SIZE,
                SIZE,  SIZE,  SIZE,
                SIZE,  SIZE,  SIZE,
                SIZE,  SIZE, -SIZE,
                SIZE, -SIZE, -SIZE,

                -SIZE, -SIZE,  SIZE,
                -SIZE,  SIZE,  SIZE,
                SIZE,  SIZE,  SIZE,
                SIZE,  SIZE,  SIZE,
                SIZE, -SIZE,  SIZE,
                -SIZE, -SIZE,  SIZE,

                -SIZE,  SIZE, -SIZE,
                SIZE,  SIZE, -SIZE,
                SIZE,  SIZE,  SIZE,
                SIZE,  SIZE,  SIZE,
                -SIZE,  SIZE,  SIZE,
                -SIZE,  SIZE, -SIZE,

                -SIZE, -SIZE, -SIZE,
                -SIZE, -SIZE,  SIZE,
                SIZE, -SIZE, -SIZE,
                SIZE, -SIZE, -SIZE,
                -SIZE, -SIZE,  SIZE,
                SIZE, -SIZE,  SIZE
        };
    }
}
