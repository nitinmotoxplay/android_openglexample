package com.example.openglexample.Z_GameEngine.Skybox;

import android.content.Context;
import android.opengl.Matrix;
import android.renderscript.Matrix4f;

import com.example.openglexample.Z_GameEngine.Entities.Camera;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector3f;
import com.example.openglexample.Z_GameEngine.Shaders.ShaderProgram;
import com.example.openglexample.Z_GameEngine.Toolbox.Maths;

/**
 * Created by Nitin Khurana on 13/11/18.
 */
public class SkyBoxShader extends ShaderProgram{

    private static final String VERTEX_SHADER = "shaders/sky_box/vertexShader.glsl";
    private static final String FRAGMENT_SHADER = "shaders/sky_box/fragmentShader.glsl";
    private static final float ROTATION_SPEED = 1f;

    private int location_projectionMatrix;
    private int location_viewMatrix;
    private int location_fogColor;
    private int location_blendFactor;
    private int location_cubeMap;
    private int location_cubeMap2;

    private float rotation;

    public SkyBoxShader(Context context) {
        super(context, VERTEX_SHADER, FRAGMENT_SHADER);
    }

    @Override
    protected void bindAttributes() {

        super.bindAttribute(0, "position");
    }

    @Override
    protected void getAllUniformLocations() {

        location_projectionMatrix = super.getUniformLocation("projectionMatrix");
        location_viewMatrix = super.getUniformLocation("viewMatrix");
        location_fogColor = super.getUniformLocation("fogColor");

        location_cubeMap = super.getUniformLocation("cubeMap");
        location_cubeMap2 = super.getUniformLocation("cubeMap2");
        location_blendFactor = super.getUniformLocation("blendFactor");
    }

    public void loadProjectionMatrix(Matrix4f matrix)  {
        super.loadMatrix(location_projectionMatrix, matrix);
    }

    public void loadBlendFactor(float factor) {

        super.loadFloat(location_blendFactor, factor);
    }

    public void connectTextureUnits() {
        super.loadInt(location_cubeMap, 0);
        super.loadInt(location_cubeMap2, 1);
    }

    public void loadViewMatrix(Camera camera)  {
        Matrix4f matrix = Maths.createViewMatrix(camera);
        matrix.set(3, 0, 0);
        matrix.set(3, 1, 0);
        matrix.set(3, 2, 0);

        rotation += ROTATION_SPEED;
        float[] matrixArray = matrix.getArray();
        Matrix.rotateM(matrixArray, 0, (float) Math.toRadians(rotation), 0, 1, 0);
        matrix = new Matrix4f(matrixArray);

        super.loadMatrix(location_viewMatrix, matrix);
    }

    public void loadFogColor(Vector3f color) {
        super.loadVector(location_fogColor, color);
    }
}
