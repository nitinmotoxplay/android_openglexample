package com.example.openglexample.Z_GameEngine.Controller;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.openglexample.R;

/**
 * Created by Nitin Khurana on 05/11/18.
 */
public class BasicButtonController extends RelativeLayout{

    public BasicButtonController(Context context, OnTouchListener onTouchListener) {
        super(context);
        init(context, onTouchListener);
    }

    public BasicButtonController(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public BasicButtonController(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        Context context1 = context;
    }

    private void init(Context context, OnTouchListener onTouchListener) {
        init(context);

        View view = LayoutInflater.from(context).inflate(R.layout.controller, this, false);
        addView(view);

        view.findViewById(R.id.forward).setOnTouchListener(onTouchListener);
        view.findViewById(R.id.backward).setOnTouchListener(onTouchListener);
        view.findViewById(R.id.left).setOnTouchListener(onTouchListener);
        view.findViewById(R.id.right).setOnTouchListener(onTouchListener);

        view.findViewById(R.id.jump).setOnTouchListener(onTouchListener);
        view.findViewById(R.id.action2).setOnTouchListener(onTouchListener);
        view.findViewById(R.id.pitch).setOnTouchListener(onTouchListener);
        view.findViewById(R.id.zoom).setOnTouchListener(onTouchListener);
    }

}
