package com.example.openglexample.Z_GameEngine.Shaders;

import android.content.Context;
import android.renderscript.Matrix4f;

import com.example.openglexample.Z_GameEngine.Entities.Camera;
import com.example.openglexample.Z_GameEngine.Entities.Light;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector3f;
import com.example.openglexample.Z_GameEngine.Toolbox.Maths;

import java.util.List;

/**
 * Created by Nitin Khurana on 05/11/18.
 */
public class TerrainShader extends ShaderProgram{

    private static final int MAX_LIGHTS = 4;
    private static final String VERTEX_FILE = "shaders/terrain_shader/vertexShader.glsl";
    private static final String FRAGMENT_FILE = "shaders/terrain_shader/fragmentShader.glsl";

    private int location_transformationMatrix;
    private int location_projectionMatrix;
    private int location_viewMatrix;
    private int location_lightPosition[];
    private int location_lightColor[];
    private int location_lightAttenuation[];
    private int location_shineDamper;
    private int location_reflectivity;
    private int location_cameraPosition;
    private int location_skyColor;
    private int location_maxLights;

    private int location_backgroundTexture;
    private int location_rTexture;
    private int location_gTexture;
    private int location_bTexture;
    private int location_blendMap;

    private int location_gradient;
    private int location_density;

    public TerrainShader(Context context) {
        super(context, VERTEX_FILE, FRAGMENT_FILE);
    }

    @Override
    protected void bindAttributes() {
        bindAttribute(0, "position");
        bindAttribute(1, "textureCoords");
        bindAttribute(2, "normal");
    }

    @Override
    protected void getAllUniformLocations() {
        location_transformationMatrix = super.getUniformLocation("transformationMatrix");
        location_projectionMatrix = super.getUniformLocation("projectionMatrix");
        location_viewMatrix = super.getUniformLocation("viewMatrix");

        location_maxLights = super.getUniformLocation("maxLights");

        location_lightPosition = new int[MAX_LIGHTS];
        location_lightColor = new int[MAX_LIGHTS];
        location_lightAttenuation = new int[MAX_LIGHTS];

        for(int i=0; i<MAX_LIGHTS; i++) {

            location_lightPosition[i] = super.getUniformLocation("lightPosition[" + i + "]");
            location_lightColor[i] = super.getUniformLocation("lightColor[" + i + "]");
            location_lightAttenuation[i] = super.getUniformLocation("lightAttenuation[" + i + "]");
        }

        location_shineDamper = super.getUniformLocation("shineDamper");
        location_reflectivity = super.getUniformLocation("reflectivity");
        location_cameraPosition = super.getUniformLocation("cameraPosition");

        location_skyColor = super.getUniformLocation("skyColor");

        location_backgroundTexture = super.getUniformLocation("backgroundTexture");
        location_rTexture = super.getUniformLocation("rTexture");
        location_gTexture = super.getUniformLocation("gTexture");
        location_bTexture = super.getUniformLocation("bTexture");
        location_blendMap = super.getUniformLocation("blendMap");

        location_gradient = super.getUniformLocation("gradient");
        location_density = super.getUniformLocation("density");
    }

    public void loadTransformationMatrix(Matrix4f matrix) {
        super.loadMatrix(location_transformationMatrix, matrix);
    }

    public void loadProjectionMatrix(Matrix4f matrix) {
        super.loadMatrix(location_projectionMatrix, matrix);
    }

    public void loadViewMatrix(Camera camera) {
        super.loadMatrix(location_viewMatrix, Maths.createViewMatrix(camera));
        super.loadVector(location_cameraPosition, new Vector3f(
                camera.getPosition()[0], camera.getPosition()[1], camera.getPosition()[2]));

    }

    public void loadSkyColor(Vector3f skyColor) {
        super.loadVector(location_skyColor, skyColor);
    }

    public void loadLights(List<Light> lights) {
        super.loadInt(location_maxLights, MAX_LIGHTS);

        for(int i=0; i<MAX_LIGHTS; i++) {

            if(i < lights.size()) {

                super.loadVector(location_lightPosition[i], lights.get(i).getPosition());
                super.loadVector(location_lightColor[i], lights.get(i).getColor());
                super.loadVector(location_lightAttenuation[i], lights.get(i).getAttenuation());
            }
            else{
                // less lights provided than MAX_LIGHTS

                super.loadVector(location_lightPosition[i], new Vector3f(0, 0, 0));
                super.loadVector(location_lightColor[i], new Vector3f(0, 0, 0));
                super.loadVector(location_lightAttenuation[i], new Vector3f(1, 0, 0));
            }
        }
    }

    public void loadShineVariables(float damper, float reflectivity) {
        super.loadFloat(location_shineDamper, damper);
        super.loadFloat(location_reflectivity, reflectivity);
    }

    public void connectTextureUnits() {
        super.loadInt(location_backgroundTexture, 0);
        super.loadInt(location_rTexture, 1);
        super.loadInt(location_gTexture, 2);
        super.loadInt(location_bTexture, 3);
        super.loadInt(location_blendMap, 4);
    }

    public void loadFog(float density, float gradient) {
        super.loadFloat(location_density, density);
        super.loadFloat(location_gradient, gradient);
    }
}
