package com.example.openglexample.Z_GameEngine.Models;

import com.example.openglexample.Z_GameEngine.Textures.ModelTexture;

/**
 * Created by Nitin Khurana on 03/11/18.
 */
public class TexturedModel {

    private RawModel rawModel;
    private ModelTexture texture;

    public TexturedModel(RawModel rawModel, ModelTexture texture) {

        this.rawModel = rawModel;
        this.texture = texture;
    }

    public RawModel getRawModel() {
        return rawModel;
    }

    public ModelTexture getTexture() {
        return texture;
    }
}
