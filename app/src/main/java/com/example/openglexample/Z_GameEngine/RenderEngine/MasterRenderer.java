package com.example.openglexample.Z_GameEngine.RenderEngine;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLES30;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.renderscript.Matrix4f;
import android.view.MotionEvent;

import com.example.openglexample.Z_GameEngine.Entities.Camera;
import com.example.openglexample.Z_GameEngine.Entities.Light;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector2f;
import com.example.openglexample.Z_GameEngine.MathClasses.Vector3f;
import com.example.openglexample.Z_GameEngine.RenderEngine.Renderers.EntityRenderer;
import com.example.openglexample.Z_GameEngine.RenderEngine.Renderers.TerrainRenderer;
import com.example.openglexample.Z_GameEngine.Skybox.SkyBoxRenderer;
import com.example.openglexample.Z_GameEngine.Water.WaterRenderer;
import com.example.openglexample.Z_GameEngine.gui.GuiRenderer;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Nitin Khurana on 31/10/18.
 */
public class MasterRenderer implements GLSurfaceView.Renderer{

    private static final float FOV = 70;
    private static final float NEAR_PLANE = 0.1f;
    private static final float FAR_PLANE = 2000;

    // sky color
    private static final float RED = 0.544f;
    private static final float GREEN = 0.62f;
    private static final float BLUE = 0.69f;

    // fog
    private final float FOG_DENSITY = 0.0035f;
    private final float FOG_GRADIENT = 4.0f;

    private EntityRenderer entityRenderer;
    private TerrainRenderer terrainRenderer;
    private SkyBoxRenderer skyBoxRenderer;
    private WaterRenderer waterRenderer;
    private GuiRenderer guiRenderer;

    private Camera camera;

    private long lastFrameTime;
    private static float delta;

//    private MousePicker mousePicker;

    public MasterRenderer(Context context, Vector2f screenSize) {

        List<Light> lights = new ArrayList<>();
//        Light sun = new Light(new Vector3f(0, 250, -400), new Vector3f(1f, 1f, 1f));
        Light sun = new Light(new Vector3f(0, 500, 400), new Vector3f(0.2f, 0.2f, 0.2f));

        lights.add(sun);

        camera = new Camera(new Vector3f(0, 10, 0));
        camera.setPitch(30);

        terrainRenderer = new TerrainRenderer(context, camera, lights);
        entityRenderer = new EntityRenderer(context, camera, lights, terrainRenderer);
        skyBoxRenderer = new SkyBoxRenderer(context, camera, lights, this);
        waterRenderer = new WaterRenderer(context, camera, lights);
        guiRenderer = new GuiRenderer(context, camera);

//        mousePicker = new MousePicker(camera, screenSize);
    }


    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {

        GLES30.glEnable(GLES30.GL_DEPTH_TEST);
        enableCulling();
        GLES20.glClearColor(RED, GREEN, BLUE, 1.0f);

        terrainRenderer.onPrepare();
        entityRenderer.onPrepare();
        skyBoxRenderer.onPrepare();
        waterRenderer.onPrepare();
        guiRenderer.onPrepare();

        entityRenderer.loadFog(FOG_DENSITY, FOG_GRADIENT);
        terrainRenderer.loadFog(FOG_DENSITY, FOG_GRADIENT);

        lastFrameTime = System.currentTimeMillis();
    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int width, int height) {
        GLES20.glViewport(0, 0, width, height);

        float[] projectionMatrixArray = new float[16];
        Matrix.setIdentityM(projectionMatrixArray, 0);
        Matrix.perspectiveM(projectionMatrixArray, 0, FOV, ((float)width)/height, NEAR_PLANE, FAR_PLANE);

        Matrix4f projectionMatrix = new Matrix4f(projectionMatrixArray);
        terrainRenderer.onSurfaceChanged(projectionMatrix, new Vector3f(RED, GREEN, BLUE));
        entityRenderer.onSurfaceChanged(projectionMatrix, new Vector3f(RED, GREEN, BLUE));
        skyBoxRenderer.onSurfaceChanged(projectionMatrix, new Vector3f(RED, GREEN, BLUE));
        waterRenderer.onSurfaceChanged(projectionMatrix, new Vector3f(RED, GREEN, BLUE));
        guiRenderer.onSurfaceChanged(projectionMatrix, new Vector3f(RED, GREEN, BLUE));

//        mousePicker.setProjectionMatrix(projectionMatrix);
    }

    @Override
    public void onDrawFrame(GL10 gl10) {
        long currentFrameTime = System.currentTimeMillis();
        delta = (currentFrameTime - lastFrameTime) / 1000f;
        lastFrameTime = currentFrameTime;

        camera.update();
        // mousePicker.update();

        renderScene();

        guiRenderer.onDrawFrame();
    }

    private void renderScene() {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        terrainRenderer.onDrawFrame();
        entityRenderer.onDrawFrame();
        skyBoxRenderer.onDrawFrame();
        waterRenderer.onDrawFrame();
    }

    public static float getFrameTimeSeconds() {
        return delta;
    }

    public void onInputReceived(float angle, MotionEvent event, int controlId) {

        terrainRenderer.onInputReceived(angle, event, controlId);
        entityRenderer.onInputReceived(angle, event, controlId);
        skyBoxRenderer.onInputReceived(angle, event, controlId);
        waterRenderer.onInputReceived(angle, event, controlId);
        camera.checkInputs(angle, controlId);
//        mousePicker.onInputReceived(event);

    }

    public static void enableCulling() {

        GLES30.glEnable(GLES20.GL_CULL_FACE);
        GLES30.glCullFace(GLES20.GL_BACK);
    }

    public static void disableCulling() {

        GLES30.glDisable(GLES20.GL_CULL_FACE);
    }

    public void cleanUp(){

        terrainRenderer.cleanUp();
        entityRenderer.cleanUp();
        skyBoxRenderer.cleanUp();
        waterRenderer.cleanUp();
        guiRenderer.cleanUp();
    }

    public void setFog(float density, float gradient) {

        entityRenderer.loadFog(density, gradient);
        terrainRenderer.loadFog(density, gradient);
    }
}
