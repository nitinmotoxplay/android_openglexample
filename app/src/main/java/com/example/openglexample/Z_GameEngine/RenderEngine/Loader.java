package com.example.openglexample.Z_GameEngine.RenderEngine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLES30;
import android.opengl.GLUtils;

import com.example.openglexample.Z_GameEngine.Models.RawModel;
import com.example.openglexample.Z_GameEngine.Textures.TextureUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nitin Khurana on 31/10/18.
 */
public class Loader {

    private List<Integer> vaos = new ArrayList<>();
    private List<Integer> vbos = new ArrayList<>();
    private List<Integer> textures = new ArrayList<>();

    public RawModel loadToVao(float[] positions, float[] textureCoords, float[] normals, int[] indices) {

        int vaoId = createVAO();
        bindIndicesBuffer(indices);
        storeDatInAttributeList(0, 3, positions);
        storeDatInAttributeList(1, 2, textureCoords);
        storeDatInAttributeList(2, 3, normals);
        unbindVAO();

        return new RawModel(vaoId, indices.length);
    }

    public RawModel loadToVao(float[] positions, int dimensions) {
        int vaoId = createVAO();
        this.storeDatInAttributeList(0, dimensions, positions);
        unbindVAO();
        return new RawModel(vaoId, positions.length / dimensions);
    }

    public void cleanUp() {

        for (int vao: vaos) {
            GLES30.glDeleteVertexArrays(1, new int[]{vao}, 0);
        }

        for (int vbo: vbos) {
            GLES30.glDeleteBuffers(1, new int[] {vbo}, 0);
        }

        for (int texture: textures) {
            GLES20.glDeleteTextures(1, new int[] {texture}, 0);
        }
    }

    public int loadTexture(Context context, int res_id) {

        int textureId = TextureUtils.loadTexture(context, res_id);

        GLES30.glGenerateMipmap(GLES20.GL_TEXTURE_2D);
        GLES30.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES30.GL_TEXTURE_MIN_FILTER, GLES30.GL_LINEAR_MIPMAP_NEAREST);
        GLES30.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES30.GL_MAX_TEXTURE_LOD_BIAS, -0.4f);

        textures.add(textureId);
        return textureId;
    }

    public int loadCubeMap(Context context, int[] textureFiles) {

        int[] textures = new int[1];
        GLES30.glGenTextures(1, textures, 0);
        int texId = textures[0];

        GLES30.glBindTexture(GLES30.GL_TEXTURE_CUBE_MAP, texId);

        for(int i=0; i<textureFiles.length; i++) {

            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), textureFiles[i]);

            GLUtils.texImage2D(GLES20.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
                    0, bitmap, 0);

        }


        GLES20.glGenerateMipmap( GLES20.GL_TEXTURE_CUBE_MAP );

        GLES20.glTexParameteri( GLES20.GL_TEXTURE_CUBE_MAP, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST );
        GLES20.glTexParameteri( GLES20.GL_TEXTURE_CUBE_MAP, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST );
        GLES20.glTexParameteri( GLES20.GL_TEXTURE_CUBE_MAP, GLES20.GL_TEXTURE_WRAP_S,     GLES20.GL_CLAMP_TO_EDGE        );
        GLES20.glTexParameteri( GLES20.GL_TEXTURE_CUBE_MAP, GLES20.GL_TEXTURE_WRAP_T,     GLES20.GL_CLAMP_TO_EDGE        );

        this.textures.add(texId);
        return texId;
    }

    private int createVAO() {

        int vaoIds[] = new int[1];
        GLES30.glGenVertexArrays(1, vaoIds, 0);
        int vaoId = vaoIds[0];
        vaos.add(vaoId);

        GLES30.glBindVertexArray(vaoId);
        return vaoId;
    }

    private void bindIndicesBuffer(int[] indices) {

        int vboIds[] = new int[1];
        GLES30.glGenBuffers(1, vboIds, 0);
        int vboId = vboIds[0];
        vbos.add(vboId);
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, vboId);

        IntBuffer buffer = storeDatInIntBuffer(indices);

        GLES30.glBufferData(GLES20.GL_ELEMENT_ARRAY_BUFFER,
                indices.length * 4, buffer, GLES20.GL_STATIC_DRAW);

//        GLES30.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);

    }

    private void storeDatInAttributeList(int attribNumber, int coordinateSize, float[] data) {

        int vboIds[] = new int[1];
        GLES30.glGenBuffers(1, vboIds, 0);
        int vboId = vboIds[0];
        vbos.add(vboId);
        GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vboId);

        FloatBuffer buffer = storeDataInFloatBuffer(data);

        GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER,
                data.length * 4, buffer, GLES30.GL_STATIC_DRAW);
        GLES30.glEnableVertexAttribArray(attribNumber);
        GLES30.glVertexAttribPointer(attribNumber, coordinateSize, GLES30.GL_FLOAT, false, 0, 0);
        GLES30.glDisableVertexAttribArray(attribNumber);
        GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, 0);

    }

    private void unbindVAO () {

        GLES30.glBindVertexArray(0);
    }

    private IntBuffer storeDatInIntBuffer(int[] data) {

        ByteBuffer buffer = ByteBuffer.allocateDirect(data.length * 4);
        buffer.order(ByteOrder.nativeOrder());
        IntBuffer buffer1 = buffer.asIntBuffer();
        buffer1.put(data);
        buffer1.position(0);

        return buffer1;
    }

    private FloatBuffer storeDataInFloatBuffer(float[] data) {

        ByteBuffer byteBuf = ByteBuffer.allocateDirect(data.length * 4); //4 bytes per float
        byteBuf.order(ByteOrder.nativeOrder());
        FloatBuffer buffer = byteBuf.asFloatBuffer();
        buffer.put(data);
        buffer.position(0);

        return buffer;
    }

}
