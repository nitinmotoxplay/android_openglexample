package com.example.openglexample.Z_GameEngine.Water;

import android.content.Context;
import android.renderscript.Matrix4f;

import com.example.openglexample.Z_GameEngine.Entities.Camera;
import com.example.openglexample.Z_GameEngine.Shaders.ShaderProgram;
import com.example.openglexample.Z_GameEngine.Toolbox.Maths;


public class WaterShader extends ShaderProgram {

	private static final String VERTEX_FILE = "shaders/water/vertexShader.glsl";
	private static final String FRAGMENT_FILE = "shaders/water/fragmentShader.glsl";

	private int location_modelMatrix;
	private int location_viewMatrix;
	private int location_projectionMatrix;

	public WaterShader(Context context) {
		super(context, VERTEX_FILE, FRAGMENT_FILE);
	}

	@Override
	protected void bindAttributes() {
		bindAttribute(0, "position");
	}

	@Override
	protected void getAllUniformLocations() {
		location_projectionMatrix = getUniformLocation("projectionMatrix");
		location_viewMatrix = getUniformLocation("viewMatrix");
		location_modelMatrix = getUniformLocation("modelMatrix");
	}

	public void loadProjectionMatrix(Matrix4f projection) {
		loadMatrix(location_projectionMatrix, projection);
	}
	
	public void loadViewMatrix(Camera camera){
		Matrix4f viewMatrix = Maths.createViewMatrix(camera);
		loadMatrix(location_viewMatrix, viewMatrix);
	}

	public void loadModelMatrix(Matrix4f modelMatrix){
		loadMatrix(location_modelMatrix, modelMatrix);
	}

}
