package com.example.openglexample.Z_GameEngine.Models;

import android.view.MotionEvent;

import com.example.openglexample.R;
import com.example.openglexample.Z_GameEngine.Entities.Entity;
import com.example.openglexample.Z_GameEngine.RenderEngine.MasterRenderer;
import com.example.openglexample.Z_GameEngine.Terrain.Terrain;

/**
 * Created by Nitin Khurana on 07/11/18.
 */
public class Player extends Entity{

    private static final float RUN_SPEED = 20;
    private static final float TURN_SPEED = 160;
    private static final float GRAVITY = -50;
    private static final float JUMP_POWER = 30;

    private float currentSpeed = 0;
    private float currentTurnSpeed = 0;
    private float upwardSpeed = 0;

    private boolean isInAir = false;

    public Player(TexturedModel model, float[] position, float[] rotation, float scale) {
        super(model, position, rotation, scale);
    }

    public void update(Terrain terrain) {

        increaseRoation(0, currentTurnSpeed * MasterRenderer.getFrameTimeSeconds(), 0);
        float distance = currentSpeed * MasterRenderer.getFrameTimeSeconds();
        float dx = (float) (distance * Math.sin(Math.toRadians(getRotation()[1])));
        float dz = (float) (distance * Math.cos(Math.toRadians(getRotation()[1])));

        increasePosition(dx, 0, dz);
        upwardSpeed += GRAVITY * MasterRenderer.getFrameTimeSeconds();
        increasePosition(0, upwardSpeed * MasterRenderer.getFrameTimeSeconds(), 0);

        float terrainHeight = terrain.getHeightOfTerrain(getPosition()[0], getPosition()[2]);

        if(getPosition()[1] < terrainHeight) {
            upwardSpeed = 0;
            getPosition()[1] = terrainHeight;
            isInAir = false;
        }
    }

    private void jump() {
        if(isInAir) return;

        this.upwardSpeed = JUMP_POWER;
        isInAir = true;
    }

    public void checkInputs(int controlId, MotionEvent event) {

        if (controlId == R.id.forward) {
            currentSpeed = RUN_SPEED;

        }
        else if(controlId == R.id.backward) {
            currentSpeed = -RUN_SPEED;
        }


        if(controlId == R.id.left) {
            currentTurnSpeed = TURN_SPEED;
        }
        else if(controlId == R.id.right) {
            currentTurnSpeed = -TURN_SPEED;
        }


        if(controlId == R.id.jump) {
            jump();
        }




        if(event.getAction() == MotionEvent.ACTION_UP) {
            currentSpeed = 0;
            currentTurnSpeed = 0;
        }

    }
}
