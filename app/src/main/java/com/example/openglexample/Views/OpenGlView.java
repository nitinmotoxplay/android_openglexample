package com.example.openglexample.Views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.opengl.GLSurfaceView;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;

import com.example.openglexample.Z_GameEngine.MathClasses.Vector2f;
import com.example.openglexample.Z_GameEngine.RenderEngine.MasterRenderer;

/**
 * Created by Nitin Khurana on 31/10/18.
 */
public class OpenGlView extends GLSurfaceView implements View.OnTouchListener{

    private static final float TOUCH_SCALE_FACTOR = 180.0f / 320;
    private float mPreviousX;
    private float mPreviousY;

    private MasterRenderer renderer;
    private Vector2f screenSize;
    float delta = 0;

    private int controller = 0; // 0 = nothing, 1 = buttons, -1 = touch screen

    public OpenGlView(Context context) {
        super(context);

        setEGLContextClientVersion(2);
        setPreserveEGLContextOnPause(true);

        Display display = ((AppCompatActivity)context).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        screenSize = new Vector2f(size.x, size.y);

        renderer = new MasterRenderer(context, screenSize);

        setRenderer(renderer);
    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        renderer.cleanUp();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent e) {

        float x = e.getX();
        float y = e.getY();

        delta = 0;

        switch (e.getAction()) {
            case MotionEvent.ACTION_MOVE:

                float dx = x - mPreviousX;
                float dy = y - mPreviousY;

                // reverse direction of rotation above the mid-line
                if (y > getHeight() / 2) {
                    dx = dx * -1 ;
                }

                // reverse direction of rotation to left of the mid-line
                if (x < getWidth() / 2) {
                    dy = dy * -1 ;
                }

                delta += ((dx + dy) * TOUCH_SCALE_FACTOR);

                if(controller == -1 || controller == 0)
                    renderer.onInputReceived(delta, null, -1);

                requestRender();

                controller = -1;
        }

        if(MotionEvent.ACTION_UP == e.getAction()) {
            controller = 0;
        }

        mPreviousX = x;
        mPreviousY = y;
        return true;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        controller = 1;
        renderer.onInputReceived(delta, event, v.getId());

        return true;
    }
}
