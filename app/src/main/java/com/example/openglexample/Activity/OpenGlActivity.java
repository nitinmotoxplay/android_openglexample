package com.example.openglexample.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.example.openglexample.R;
import com.example.openglexample.Views.OpenGlView;
import com.example.openglexample.Z_GameEngine.Controller.BasicButtonController;

/**
 * Created by Nitin Khurana on 31/10/18.
 */
public class OpenGlActivity extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.game_view);

        FrameLayout root = findViewById(R.id.root);

        OpenGlView openGlView = new OpenGlView(this);
        root.addView(openGlView);

        BasicButtonController basicButtonController = new BasicButtonController(this, openGlView);
        root.addView(basicButtonController);
    }
}
