precision mediump float;

varying vec3 textureCoords;

uniform vec3 fogColor;
uniform samplerCube cubeMap;
uniform samplerCube cubeMap2;
uniform float blendFactor;

const float lowerLimit = 0.0;
const float upperLimit = 30.0;

void main(void)
{
    vec4 texture1 = textureCube(cubeMap, textureCoords);
    vec4 texture2 = textureCube(cubeMap2, textureCoords);

    vec4 finalColor = mix(texture1, texture2, blendFactor);

    float factor = (textureCoords.y - lowerLimit) / (upperLimit - lowerLimit);
    factor = clamp(factor, 0.0, 1.0);

    gl_FragColor = mix(vec4(fogColor, 1.0), finalColor, factor);
}