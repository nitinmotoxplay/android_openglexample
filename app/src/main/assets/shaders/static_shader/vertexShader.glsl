
attribute vec3 position;
attribute vec2 textureCoords;
attribute vec3 normal;

varying vec2 pass_textureCoords;
varying vec3 surfaceNormal;
varying vec3 toLightVector[4];
varying vec3 toCameraVector;
varying float visibility;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

uniform vec3 lightPosition[4];
uniform vec3 cameraPosition;

uniform float useFakeLighting;

uniform float numberOfRows;
uniform int maxLights1;
uniform vec2 offset;

uniform float density;
uniform float gradient;

//const float density = 0.0035;
//const float gradient = 4.0;

//const float density = 0.0;
//const float gradient = 1.0;

 void main(void) {

    vec4 worldPosition = transformationMatrix * vec4(position, 1.0);
    vec4 positionRelativeToCamera = viewMatrix * worldPosition;

    gl_Position = projectionMatrix * positionRelativeToCamera;
    pass_textureCoords = (textureCoords/numberOfRows) + offset;

    vec3 actualNormal = normal;
    if (useFakeLighting > 0.5) {
        actualNormal = vec3(0.0, 1.0, 0.0);
    }

    surfaceNormal = (transformationMatrix * vec4(actualNormal, 0.0)).xyz;

    for(int i=0; i<4; i++) {
        toLightVector[i] = lightPosition[i] - worldPosition.xyz;
    }

//    toCameraVector = (inverse(viewMatrix) * vec4(0.0,0.0,0.0,1.0)).xyz - worldPosition.xyz;

    toCameraVector = cameraPosition - worldPosition.xyz;

    float distance = length(positionRelativeToCamera.xyz);
    visibility = exp(-pow((distance * density), gradient));
    visibility = clamp(visibility, 0.0, 1.0);

 }
