precision mediump float;

varying vec2 pass_textureCoords;
varying vec2 tiledCoords;
varying vec3 surfaceNormal;
varying vec3 toLightVector[4];
varying vec3 toCameraVector;
varying float visibility;

uniform sampler2D backgroundTexture;
uniform sampler2D rTexture;
uniform sampler2D gTexture;
uniform sampler2D bTexture;
uniform sampler2D blendMap;

uniform vec3 lightColor[4];
uniform vec3 lightAttenuation[4];

uniform float shineDamper;
uniform float reflectivity;
uniform vec3 skyColor;
uniform int maxLights;

void main(void) {

    vec4 blendMapColor = texture2D(blendMap, pass_textureCoords);
    float backTextureAmount = 1.0 - (blendMapColor.r + blendMapColor.g + blendMapColor.b);
    vec4 backgroundTextureColor = texture2D(backgroundTexture, tiledCoords) * backTextureAmount;
    vec4 rTextureColor = texture2D(rTexture, tiledCoords) * blendMapColor.r;
    vec4 gTextureColor = texture2D(gTexture, tiledCoords) * blendMapColor.g;
    vec4 bTextureColor = texture2D(bTexture, tiledCoords) * blendMapColor.b;

    vec4 totalColor = backgroundTextureColor + rTextureColor + gTextureColor + bTextureColor;

    vec3 unitNormal = normalize(surfaceNormal);
    vec3 unitToCameraVector = normalize(toCameraVector);

    vec3 totalDiffuse = vec3(0.0, 0.0, 0.0);
    vec3 totalSpecular = vec3(0.0, 0.0, 0.0);

    for(int i=0; i<maxLights; i++) {

        float distance = length(toLightVector[i]);
        float attFactor = lightAttenuation[i].x + (lightAttenuation[i].y * distance) + (lightAttenuation[i].z * distance * distance);

        vec3 unitToLightVector = normalize(toLightVector[i]);

        float nDot1 = dot(unitNormal, unitToLightVector);
        float brightNess = max(nDot1, 0.0);

        vec3 lightDirection = -unitToLightVector;
        vec3 reflectedLightDirection = reflect(lightDirection, unitNormal);

        float specularFactor = dot(reflectedLightDirection, unitToCameraVector);
        specularFactor = max(specularFactor, 0.0);
        float dampedFactor = pow(specularFactor, shineDamper);

        vec3 diffuse = brightNess * lightColor[i];
        vec3 finalSpecular = dampedFactor * reflectivity * lightColor[i];

        totalDiffuse = totalDiffuse + diffuse/attFactor;
        totalSpecular = totalSpecular + finalSpecular/attFactor;
    }

    totalDiffuse = max(totalDiffuse, 0.2);

    vec4 outColor = vec4(totalDiffuse, 1.0) * totalColor + vec4(totalSpecular, 1.0);

    outColor = mix(vec4(skyColor, 1.0), outColor, visibility);

    gl_FragColor = outColor;
}
