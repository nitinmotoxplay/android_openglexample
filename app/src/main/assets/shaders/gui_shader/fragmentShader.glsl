precision mediump float;

varying vec2 textureCoords;
uniform sampler2D texture;

void main(void)
{
    gl_FragColor = texture2D(texture, textureCoords);
}